                  #include Wire.h
                  #include Adafruit_PWMServoDriver.h
                
                  Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver(0x40);
                  Adafruit_PWMServoDriver pwm2 = Adafruit_PWMServoDriver(0x41);
                
                  #define SERVOMIN  125 // this is the 'minimum' pulse length count (out of 4096)
                  #define SERVOMAX  600 // this is the 'maximum' pulse length count (out of 4096)
                  uint8_t servonum = 0;    
                         
                  void setup() {
                  Serial.begin(9600);
                  Serial.println("16 channel Servo test!");
                
                  pwm.begin();
                  pwm2.begin();
                  
                  pwm.setPWMFreq(60);  // Analog servos run at ~60 Hz updates
                  pwm2.setPWMFreq(60);
                
                  //Modo padrão
                
                  pata2(70, 80, 20);
                  pata3(60, 70, 110);
                  pata6(90, 80, 20);
                  pata1(90, 70, 90);
                  pata4(60, 80, 20);
                  pata5(70, 70, 110);
                  delay(3000);
                  
                  }
                
                  void loop() {
                  // Modo Brotinho
                  pata2(70, 0, 0);
                  pata3(60, 180, 180);
                  pata6(90, 0, 0);
                  pata1(90, 180, 180);
                  pata4(60, 0, 0);
                  pata5(70, 180, 180);
                  delay(1000);
                  //Modo padrão
                  pata2(70, 80, 20);
                  pata3(60, 70, 110);
                  pata6(90, 80, 20);
                  pata1(90, 70, 90);
                  pata4(60, 80, 20);
                  pata5(70, 70, 110);
                  delay(1000);
                  // Marchinha
                  for(int i=0; i<4; i++){
                  pata1(90, 70, 90);
                  pata4(60, 80, 20);
                  pata5(70, 70, 110);
                  
                  pata2(70, 60, 50);
                  pata6(90, 60, 50);
                  pata3(20 ,120, 130);
                  delay(100);
                  pata2(70, 80, 20);
                  pata3(60, 70, 110);
                  pata6(90, 80, 20);
                
                  pata1(90, 120, 130);
                  pata4(60, 60, 50);
                  pata5(70, 120, 130);
                  
                  delay(100);
                  }
                  // Modo padrão
                  pata2(70, 80, 20);
                  pata3(60, 70, 110);
                  pata6(90, 80, 20);
                
                  pata1(90, 70, 90);
                  pata4(60, 80, 20);
                  pata5(70, 70, 110);
                  delay(1000);
                  //Aceno pata 2
                  for(int i =0; i<2; i++){
                  pata2(50,40,110);
                  delay(500);
                  pata2(60,40,15);
                  delay(500);
                  }
                  delay(500);
                                 
                  }
                  void pata1 (int angulo1, int angulo2, int angulo3) {
                  pwm.setPWM(1, 1, angleToPulse(angulo1) );
                  pwm.setPWM(2, 1, angleToPulse(angulo2) );
                  pwm.setPWM(3, 1, angleToPulse(angulo3) );
                  }
                  void pata2 (int angulo1, int angulo2, int angulo3) {
                  pwm2.setPWM(1, 1, angleToPulse(angulo1) );
                  pwm2.setPWM(2, 1, angleToPulse(angulo2) );
                  pwm2.setPWM(3, 1, angleToPulse(angulo3) );
                  }
                  void pata3 (int angulo1, int angulo2, int angulo3) {
                  pwm.setPWM(5, 1, angleToPulse(angulo1) );
                  pwm.setPWM(6, 1, angleToPulse(angulo2) );
                  pwm.setPWM(7, 1, angleToPulse(angulo3) );
                  }
                  void pata4 (int angulo1, int angulo2, int angulo3) {
                  pwm2.setPWM(5, 1, angleToPulse(angulo1) );
                  pwm2.setPWM(6, 1, angleToPulse(angulo2) );
                  pwm2.setPWM(7, 1, angleToPulse(angulo3) );
                  }
                  void pata5 (int angulo1, int angulo2, int angulo3) {
                  pwm.setPWM(9, 1, angleToPulse(angulo1) );
                  pwm.setPWM(10, 1, angleToPulse(angulo2) );
                  pwm.setPWM(11, 1, angleToPulse(angulo3) );
                  }
                  void pata6 (int angulo1, int angulo2, int angulo3) {
                  pwm2.setPWM(9, 1, angleToPulse(angulo1) );
                  pwm2.setPWM(10, 1, angleToPulse(angulo2) );
                  pwm2.setPWM(11, 1, angleToPulse(angulo3) );
                  }
                
                  int angleToPulse(int ang) {
                  int pulse = map(ang, 0, 180, SERVOMIN, SERVOMAX); // map angle of 0 to 180 to Servo min and Servo max
                  Serial.print("Angle: "); Serial.print(ang);
                  Serial.print(" pulse: "); Serial.println(pulse);
                  return pulse;
                }